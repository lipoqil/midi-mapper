# frozen_string_literal: true

module Commands
  CC_MESSAGE = 0xB0
  PROGRAMMER_LIVE_MODE = 0x0E
  PROGRAM_CHANGE = 0xC0
  SCROLLING_TEXT = 0x07
end
