# MIDI mapper

Maps MIDI messages into system actions. Mainly meant for use with
[Novation Launchpad](https://novationmusic.com/en/launch).

Currently supported actions:
- switching between apps
- changing system volume

## Usage

1. Clone this repo
2. Send the custom mode to Launchpad (code is counting with using _User_ Custom Layout (Layout 3))
    - you can use [Novation Components](https://novationmusic.com/en/components),
 [SysEx Librarian](https://www.snoize.com/sysexlibrarian/) or some other
 [SysEx](https://en.wikipedia.org/wiki/MIDI#SysEx) tool 
3. Create a config _config.yaml_ file from the provided example 
4. In Terminal or other terminal app
   - Bundle the project -- `bundle`
   - run the mapper script -- `ruby main.rb`

## Dependencies

- Currently the actions relly on Apple Script, so it is pretty safe to say, you need macOS / OS X
- Code is written in Ruby, so you need a working Ruby interpreter on your machine

## Plans

- config file(s)
  - for a pad
    - color
- actions specific per app
- reflect on Launchpad changes, done without Launchpad
