# frozen_string_literal: true

require 'unimidi'
require 'yaml'
require './commands'

# Check Launchpad Mini - Programmers Reference Manual.pdf

# First, initialize the MIDI io ports
@launchpad_output = UniMIDI::Input.all[1].open
@launchpad_input = UniMIDI::Output.all[2].open
@config = YAML.load_file('config.yaml')

def handle_message(message:)
  message_type = message[:data][0]
  puts message
  case message_type
  when Commands::CC_MESSAGE
    cc_number, value = message[:data][1..2]
    handle_cc_message cc_number:, value:
  when Commands::PROGRAM_CHANGE
    program_number = message[:data][1]
    handle_program_change program_number:
  end
end

def handle_cc_message(cc_number:, value:)
  case cc_number
  when 7
    `osascript -e "set Volume #{(value / 12.7).to_i}"`
  end
end

# @param [Integer] program_number
def handle_program_change(program_number:)
  puts "Program change #{program_number}"
  action = @config['program_change'][program_number] ||
           raise(ArgumentError, "Program change (#{program_number}) is not configured")

  case action['type']
  when 'open_app'
    puts `osascript -e 'tell application "#{action['data']['name']}" to activate'`
  else
    puts "I do not handle #{action['type']} yet."
  end
end

# @param [Numeric] command Hexadecimal number
# @param [Array<Numeric>] data Array of hexadecimal numbers
#
# @see https://en.wikipedia.org/wiki/MIDI#SysEx
def send_sysex(command:, data: [])
  message = [0xF0, 0x00, 0x20, 0x29, 0x02, 0x0D]
  message << command
  message.concat data
  message << 0xF7
  puts "Sending #{message}"
  @launchpad_input.puts(message)
end

def run_action(name:, data:)
  case name
  when 'open_app'
    puts `osascript -e 'tell application "#{data}" to activate'`
  when 1
    puts `osascript -e 'tell application "Terminal" to activate'`
  when 2
    puts `osascript -e 'tell application "Components" to activate'`
  end
end

# Select Programmer mode
send_sysex command: Commands::PROGRAMMER_LIVE_MODE, data: [0x01]

# Select Custom mode: User
send_sysex command: 0x00, data: [0x06]

# Make first pad, first row active (Program 0)
send_sysex command: Commands::PROGRAM_CHANGE, data: [0x00]

# Make first pad, second row active (Program 5)
sleep 1
send_sysex command: Commands::PROGRAM_CHANGE, data: [0x05]

# Make first pad, third row active (Program 10)
sleep 1
send_sysex command: Commands::PROGRAM_CHANGE, data: [0x0A]

# Scrolling text
# send_sysex command: Commands::SCROLLING_TEXT,
#            data: [0x01, 0x07, 0x00, 0x25, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64])

terminal_width = `tput cols`.strip.to_i
puts 'Program change layout'
if terminal_width.positive?
  puts '=' * terminal_width
  3.times do |line_index|
    line_parts = (0..4).each_with_object([]) do |program_number, line|
      program_change = @config['program_change'].dig(program_number + (5 * line_index))
      type = program_change&.dig('type')
      name = program_change&.dig('data', 'name')
      pad_content = (type.nil? && name.nil?) ? '' : "#{type} #{name}"
      line << pad_content.center((terminal_width - 6) / 5)
    end
    puts line_parts.join('|')
    puts '=' * terminal_width
  end
end

begin
  puts 'Control-C to quit...'
  processed_timestamp = nil
  loop do
    last_message = @launchpad_output.gets.last
    handle_message(message: last_message) if processed_timestamp != last_message[:timestamp]
    sleep 2
  end
ensure
  puts 'Exiting...'
  # Clear scrolling text
  send_sysex command: Commands::SCROLLING_TEXT
  # Switch back to Live mode
  send_sysex command: Commands::PROGRAMMER_LIVE_MODE, data: [0x00]
end
